#!/bin/bash

#SBATCH --job-name=inter
#SBATCH --mail-user=atahiraj@student.ulg.ac.be
#SBATCH --mail-type=ALL
#SBATCH  --ntasks=1
#SBATCH  --cpus-per-task=32
#        ##################

#SBATCH --mem-per-cpu=2000
#SBATCH --time=7:45:59


gcc -fopenmp -lm -std=c99 -o app *.c

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
export OMP_STACKSIZE=100M

echo "OMP_STACKSIZE="$OMP_STACKSIZE

echo "OMP_NUM_THREADS="$OMP_NUM_THREADS
echo "\n threads : 1 2 4 8 16 32"


for nb_threads in 1 2 4 8 16 32
do
	export OMP_NUM_THREADS=$nb_threads ;
	echo "\n\n------------\n OMP_NUM_THREADS= " $OMP_NUM_THREADS ;
	time ./app 20000 5000 0.04 ;
done
