/** @file */

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <omp.h>

#ifndef M_PI
#define M_PI (3.14159265358979323846)
#endif // M_PI

#define d (346E-12)
#define dt (1E-14)
#define D (5E27)
#define epsilon (2E-16)
#define kB (1.38064852E-23)
#define m (5.314E-26)
#define qe (1.602176635E-19)
#define SEED 1234

/**
 * @brief Convenience enum to access coordinates with x, y and z
 * 
 */
enum { x = 0, y = 1, z = 2 };

/**
 * @brief Adds two vectors
 * 
 * @param v1 Augend
 * @param v2 Addend
 * @param sum double holding the result
 */
__attribute__((always_inline)) static inline void
add(const double v1[], const double v2[], double sum[])
{
    sum[x] = v1[x] + v2[x];
    sum[y] = v1[y] + v2[y];
    sum[z] = v1[z] + v2[z];
}

/**
 * @brief Multiplies a vector by a constant
 * 
 * @param v Multiplicand
 * @param a Multiplier
 * @param prod double holding the result
 */
__attribute__((always_inline)) static inline void
scale(const double v[], const double a, double prod[])
{
    prod[x] = v[x] * a;
    prod[y] = v[y] * a;
    prod[z] = v[z] * a;
}

/**
 * @brief Computes the suberence of two vectors
 * 
 * @param v1 Minuend
 * @param v2 Subtrahend
 * @param sub double holding the result
 */
__attribute__((always_inline)) static inline void
sub(const double v1[], const double v2[], double sub[])
{
    sub[x] = v1[x] - v2[x];
    sub[y] = v1[y] - v2[y];
    sub[z] = v1[z] - v2[z];
}

/**
 * @brief Computes the scalar product of 2 vectors
 * 
 * @param v1 First vector
 * @param v2 Second vector
 * @return double 
 */
__attribute__((always_inline)) static inline double scalar(const double v1[],
                                                           const double v2[])
{
    return v1[x] * v2[x] + v1[y] * v2[y] + v1[z] * v2[z];
}

/**
 * @brief Computes the length of a vector
 * 
 * @param v The vector whose length is to be computed
 * @return double 
 */
__attribute__((always_inline)) static inline double length(const double v[])
{
    return sqrt(pow(v[x], 2) + pow(v[y], 2) + pow(v[z], 2));
}

/**
 * @brief Computes the length of the suberence of 2 vectors
 * 
 * @param v1 First vector
 * @param v2 Second vector
 * @return double 
 */
__attribute__((always_inline)) static inline double dist(const double v1[],
                                                         const double v2[])
{
    double buffer[3];
    sub(v1, v2, buffer);
    return length(buffer);
}

/**
 * @brief Returns a pseudo random double between 0 and 1
 * 
 * @return double 
 */
__attribute__((always_inline)) static inline double rand1(void)
{
    return (double)rand() / (double)RAND_MAX;
}

int main(int argc, char **argv)
{
    if (argc < 4) {
        fprintf(stderr, "Error: 3 arguments must be given\n");
        exit(1);
    } else if (atoi(argv[1]) < 0 || atof(argv[2]) < 0 || atof(argv[3]) < 0) {
        fprintf(stderr, "Error: all arguments must be positive\n");
        exit(1);
    }

    double t1, t2;

    const unsigned I = (unsigned)atoi(argv[1]);
    const double l = atof(argv[2]) * 1E-12; // Converting to Joule directly
    const double K = atof(argv[3]) * qe;

    const double v_0 = sqrt(2 * K / m);
    const unsigned N = (unsigned)(ceil(D * l * l * l));

    double *velocities = (double *)malloc(3 * N * sizeof(double));
    if (velocities == NULL) {
        perror(NULL);
        exit(1);
    }

    t1 = omp_get_wtime();

    omp_lock_t *locks = (omp_lock_t *)malloc(N * sizeof(omp_lock_t));
    if (locks == NULL) {
        perror(NULL);
        exit(1);
    }

    double *positions = (double *)malloc(3 * N * sizeof(double));
    if (positions == NULL) {
        perror(NULL);
        exit(1);
    }

    FILE *fptr;
    fptr = fopen("speed.csv", "w");
    if (fptr == NULL) {
        perror(NULL);
        exit(1);
    }

    // Initializing velocitie and positions and initializing locks
    srand(SEED);
    for (unsigned i = 0; i < N; ++i) {
        double theta_i = M_PI * rand1();
        double phi_i = 2 * M_PI * rand1();
        velocities[i * 3 + x] = v_0 * sin(theta_i) * cos(phi_i);
        velocities[i * 3 + y] = v_0 * sin(theta_i) * sin(phi_i);
        velocities[i * 3 + z] = v_0 * cos(theta_i);
        positions[i * 3 + x] = l * rand1();
        positions[i * 3 + y] = l * rand1();
        positions[i * 3 + z] = l * rand1();
        omp_init_lock(&locks[i]);
    }

    for (unsigned timestep = 0; timestep < I; ++timestep) {
// Updating velocities
#pragma omp parallel for schedule(dynamic)
        for (unsigned i = 0; i < N; ++i) {
            double *v_i = &velocities[3 * i], *x_i = &positions[3 * i];

            for (unsigned j = i + 1; j < N; ++j) {
                double *v_j = &velocities[3 * j], *x_j = &positions[3 * j];
                double distance = dist(x_i, x_j);

                if (distance < d && distance > epsilon) {
                    double *v_j = &velocities[3 * j], *x_j = &positions[3 * j];
                    double sub_vel[3], sub_pos[3], change[3];
                    double multiplier;

                    // This cannot result in a deadlock
                    omp_set_lock(&locks[i]);
                    omp_set_lock(&locks[j]);

                    // Updating v_i
                    sub(x_i, x_j, sub_pos);
                    sub(v_i, v_j, sub_vel);
                    multiplier = scalar(sub_vel, sub_pos) / pow(distance, 2);
                    scale(sub_pos, multiplier, change);
                    sub(v_i, change, v_i);

                    // Updating v_j
                    add(v_j, change, v_j);

                    omp_unset_lock(&locks[i]);
                    omp_unset_lock(&locks[j]);
                }
            }
        }

// Updating positions
#pragma omp parallel for schedule(static)
        for (unsigned i = 0; i < N; ++i) {
            double *v_i = &velocities[3 * i], *x_i = &positions[3 * i];

            // Updating x_i
            double change[3];
            scale(v_i, dt, change);
            add(x_i, change, x_i);

            // Handling particles leaving the box
            for (unsigned j = 0; j < 3; ++j) {
                x_i[j] = fmod(x_i[j], l);
                if (x_i[j] < 0)
                    x_i[j] += l;
            }
        }
    }

    t2 = omp_get_wtime();

    // Computing average speed, pressure and temperature
    double average_speed = 0;
    for (unsigned i = 0; i < N; ++i) {
        double len = length(&velocities[3 * i]);
        average_speed += pow(len, 2) / N;
        fprintf(fptr, "%lf\n", len);
    }

    double pressure = D * m * average_speed / 3;
    double temperature = m * average_speed / (3 * kB);

    printf("Average square speed: %lf\n", average_speed);
    printf("Pressure: %lf\n", pressure);
    printf("Temperate: %lf\n", temperature);
    printf("Time = %g\n", t2 - t1);

    fclose(fptr);
    free(velocities);
    free(positions);
    free(locks);
}
